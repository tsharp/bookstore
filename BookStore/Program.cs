﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BookStore
{
    class Program
    {
        static void Main(string[] args)
        {
            var bookStore = LoadBooks();

            var order = new Order();
            order.AddBook(bookStore.GetBook("Unsolved murders"), 1);
            order.AddBook(bookStore.GetBook("A Little Love Story"), 1);
            order.AddBook(bookStore.GetBook("Heresy"), 1);
            order.AddBook(bookStore.GetBook("Jack the Ripper"), 1);
            order.AddBook(bookStore.GetBook("The Tolkien Years"), 1);

            Console.WriteLine("Order total ex GST:" + order.GetTotalExcludingTax());
            Console.WriteLine("Order total inc GST:" + order.GetTotalIncludingTax());
            Console.ReadKey();
        }

        public static BookStore LoadBooks()
        {
            var bookStore = new BookStore();
            bookStore.Books.Add(new Book() { Title = "Unsolved murders", Author = "Emily G. Thompson, Amber Hunt", Genre = Genre.Crime, Price = 10.99 });
            bookStore.Books.Add(new Book() { Title = "Alice in Wonderland", Author = "Lewis Carroll", Genre = Genre.Fantasy, Price = 5.99 });
            bookStore.Books.Add(new Book() { Title = "A Little Love Story", Author = "Roland Merullo", Genre = Genre.Romance, Price = 2.4 });
            bookStore.Books.Add(new Book() { Title = "Heresy", Author = "S J Parris", Genre = Genre.Fantasy, Price = 6.8 });
            bookStore.Books.Add(new Book() { Title = "The Neverending Story", Author = "Michael Ende", Genre = Genre.Fantasy, Price = 7.99 });
            bookStore.Books.Add(new Book() { Title = "Jack the Ripper", Author = "Philip Sugden", Genre = Genre.Crime, Price = 16.0 });
            bookStore.Books.Add(new Book() { Title = "The Tolkien Years", Author = "Greg Hildebrandt", Genre = Genre.Fantasy, Price = 22.9 });
            return bookStore;
        }
    }

    public enum Genre
    {
        Crime,
        Fantasy,
        Romance
    }

    public class BookStore
    {
        public List<Book> Books { get; set; }

        public BookStore () {
            Books = new List<Book>();
        }

        public Book GetBook(string title)
        {
            return Books.First(x => x.Title == title);
        }
    }

    public class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public Genre Genre { get; set; }
        public double Price { get; set; }
    }

    public class Order
    {
        public List<OrderItem> OrderItems { get; set; }
        public Order()
        {
            OrderItems = new List<OrderItem>();
        }

        public void AddBook(Book book, int quantity)
        {
            OrderItems.Add(new OrderItem() { Book = book, Quantity = quantity });
        }

        public double GetDiscount(Genre genre)
        {
            return genre == Genre.Crime ? 0.95 : 1;
        }

        public double GetTotalExcludingTax()
        {
            var orderTotal = OrderItems.Sum(x => (x.Book.Price * GetDiscount(x.Book.Genre)) * x.Quantity);
            if (orderTotal < 20) {
                orderTotal += 5.95;
            }
            return Math.Round(orderTotal, 2);
        }

        public double GetTotalIncludingTax()
        {
            return Math.Round(GetTotalExcludingTax() * 1.1, 2);
        }
    }

    public class OrderItem 
    {
        public Book Book { get; set; }
        public int Quantity { get; set; }
    }
}
